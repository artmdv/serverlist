﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using ServerList.Application.Services;
using ServerList.Domain.Identity;
using ServerList.Domain.Models;

namespace ServerList.WPF.Application.ViewModels
{
    public class ServersViewModel: MvxViewModel
    {
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private IEnumerable<Server> _servers;

        public IEnumerable<Server> Servers
        {
            get => _servers;
            private set
            {
                _servers = value;
                RaisePropertyChanged(() => Servers);
            }
        }

        private readonly IServerListService _serverListService;
        private readonly IMvxNavigationService _navigationService;

        public IMvxAsyncCommand LogoutCommand => new MvxAsyncCommand(async () => await Logout());

        private async Task Logout()
        {
            Logger.Info("Logging out.");

            try
            {
                var principal = Thread.CurrentPrincipal as CustomPrincipal;
                if (principal == null)
                    throw new ArgumentException(
                        "The application's default thread principal must be set to a CustomPrincipal object on startup.");

                principal.Identity = new AnonymousIdentity();

                await _navigationService.Navigate<LoginViewModel>();
            }
            catch (Exception ex)
            {
                Logger.Error("Error occured while logging out", ex);
            }
        }

        public ServersViewModel(IServerListService serverListService, IMvxNavigationService navigationService)
        {
            _serverListService = serverListService;
            _navigationService = navigationService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            Logger.Info("Retrieving server list.");

            try
            {
                Servers = await _serverListService.Get();
            }
            catch (Exception ex)
            {
                Logger.Error("Error retrieving server list.", ex);
            }
            
        }
    }
}
