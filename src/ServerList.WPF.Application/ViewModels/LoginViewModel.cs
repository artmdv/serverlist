﻿using System;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using ServerList.Application.Services;
using ServerList.Domain.Identity;

namespace ServerList.WPF.Application.ViewModels
{
    public class LoginViewModel: MvxViewModel
    {
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IServerListService _serverListService;
        private readonly IMvxNavigationService _navigationService;
        private string _username;
        public string Username
        {
            get => _username;
            set
            {
                _username = value;
                RaisePropertyChanged(() => Username);
            }
        }
        
        private string _password;
        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                RaisePropertyChanged(() => Password);
            }
        }
        
        private string _error;
        public string Error
        {
            get => _error;
            set
            {
                _error = value;
                RaisePropertyChanged(() => Error);
            }
        }

        public IMvxAsyncCommand LoginCommand => new MvxAsyncCommand(async ()=> await Login());

        public LoginViewModel(IServerListService serverListService, IMvxNavigationService navigationService)
        {
            _serverListService = serverListService;
            _navigationService = navigationService;
        }

        private async Task Login()
        {
            try
            {
                Logger.Info("Tying to log in"); //this should actually be metrics, not logs

                var token = await _serverListService.Authenticate(Username, Password);
                if (string.IsNullOrWhiteSpace(token))
                {
                    throw new UnauthorizedAccessException();
                }
                
                var principal = Thread.CurrentPrincipal as CustomPrincipal;
                if (principal == null)
                    throw new ArgumentException(
                        "The application's default thread principal must be set to a CustomPrincipal object on startup.");

                principal.Identity = new BearerIdentity(Username, token);

                Logger.Info("Successfully logged in.");
                await _navigationService.Navigate<ServersViewModel>();
            }
            catch (UnauthorizedAccessException ex)
            {
                Error = "Authentication failed.";
                Logger.Error("Unauthorized", ex);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Unhandled exception", ex);
            }
        }
    }
}
