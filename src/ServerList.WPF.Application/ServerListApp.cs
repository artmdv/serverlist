﻿using System;
using System.Net.Http;
using MvvmCross;
using MvvmCross.Platform.IoC;
using ServerList.Application.Services;
using ServerList.Domain.Identity;
using ServerList.Domain.Interfaces;
using ServerList.Infrastructure.Repositories;
using ServerList.WPF.Application.ViewModels;

namespace ServerList.WPF.Application
{
    public class ServerListApp : MvvmCross.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CustomPrincipal customPrincipal = new CustomPrincipal();
            AppDomain.CurrentDomain.SetThreadPrincipal(customPrincipal);

            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            
            Mvx.IoCProvider.RegisterSingleton(()=>new HttpClient());
            Mvx.IoCProvider.RegisterType<IServerRepository, ServerRepository>();
            Mvx.IoCProvider.RegisterType<IUserRepository, UserRepository>();
            Mvx.IoCProvider.RegisterType<IServerListService, ServerListService>();

            RegisterAppStart<LoginViewModel>();
        }
    }
}
