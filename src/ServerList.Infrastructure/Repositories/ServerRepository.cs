﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ServerList.Domain.Identity;
using ServerList.Domain.Interfaces;
using ServerList.Domain.Models;

namespace ServerList.Infrastructure.Repositories
{
	public class ServerRepository: IServerRepository
	{
        private readonly HttpClient _httpClient;

        public ServerRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Server>> Get()
        {
            var token = (Thread.CurrentPrincipal.Identity as BearerIdentity)?.Token;

            var request= new HttpRequestMessage(HttpMethod.Get, "http://playground.tesonet.lt/v1/servers");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var serversResponse = await _httpClient.SendAsync(request);

            if (!serversResponse.IsSuccessStatusCode)
            {
                throw new Exception($"Error occured retrieving server list. Error code: {serversResponse.StatusCode.ToString()}");
            }

            var serversJson = await serversResponse.Content.ReadAsStringAsync();
            
            var servers = JsonConvert.DeserializeObject<IEnumerable<Server>>(serversJson);
            return servers;
        }
    }
}
