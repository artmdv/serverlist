﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ServerList.Domain.Interfaces;

namespace ServerList.Infrastructure.Repositories
{
    public class UserRepository: IUserRepository
    {
        private readonly HttpClient _httpClient;

        public UserRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<string> Authenticate(string username, string password)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "http://playground.tesonet.lt/v1/tokens");
            request.Content = new StringContent(JsonConvert.SerializeObject(new {username, password}, new JsonSerializerSettings{NullValueHandling = NullValueHandling.Ignore}), Encoding.UTF8, "application/json");

            var response = await _httpClient.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                throw new UnauthorizedAccessException();
            }

            var tokenJson = await response.Content.ReadAsStringAsync();
            dynamic tokenObject = JsonConvert.DeserializeObject(tokenJson);
            return tokenObject?.token;
        }
    }
}
