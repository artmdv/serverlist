﻿namespace ServerList.Domain.Identity
{
    public class AnonymousIdentity: BearerIdentity
    {
        public AnonymousIdentity() : base(string.Empty, null)
        {
        }
    }
}
