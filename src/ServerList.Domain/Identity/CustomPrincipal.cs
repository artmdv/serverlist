﻿using System.Security.Principal;

namespace ServerList.Domain.Identity
{
    public class CustomPrincipal : IPrincipal
    {
        private BearerIdentity _identity;

        public BearerIdentity Identity
        {
            get => _identity ?? new AnonymousIdentity();
            set => _identity = value;
        }

        #region IPrincipal Members
        IIdentity IPrincipal.Identity => Identity;

        public bool IsInRole(string role)
        {
            return !string.IsNullOrEmpty(_identity.Token);
        }
        #endregion
    }
}
