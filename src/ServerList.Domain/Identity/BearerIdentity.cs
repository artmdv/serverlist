﻿using System.Security.Principal;

namespace ServerList.Domain.Identity
{
    public class BearerIdentity: IIdentity
    {
        public string AuthenticationType => "Bearer";
        public bool IsAuthenticated => !string.IsNullOrEmpty(Token);

        public string Name { get; }

        public string Token { get; set; }

        public BearerIdentity(string username, string token)
        {
            Name = username;
            Token = token;
        }
    }
}
