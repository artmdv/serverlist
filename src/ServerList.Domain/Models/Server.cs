﻿namespace ServerList.Domain.Models
{
	public class Server
	{
		public Server( string name, int distance)
		{
			Name = name;
            Distance = distance;
        }

		public int Distance { get; set; }
		public string Name { get; set; }
	}
}
