﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ServerList.Domain.Models;

namespace ServerList.Domain.Interfaces
{
	public interface IServerRepository: IRepository
	{
        Task<IEnumerable<Server>> Get();
    }
}
