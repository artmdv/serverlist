﻿using System.Threading.Tasks;

namespace ServerList.Domain.Interfaces
{
    public interface IUserRepository: IRepository
    {
        Task<string> Authenticate(string username, string password);
    }
}
