﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ServerList.Domain.Models;

namespace ServerList.Application.Services
{
    public interface IServerListService
    {
        Task<string> Authenticate(string username, string password);
        Task<IEnumerable<Server>> Get();
    }
}
