﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ServerList.Domain.Interfaces;
using ServerList.Domain.Models;

namespace ServerList.Application.Services
{
    public class ServerListService: IServerListService
    {
	    private readonly IServerRepository _serverRepository;
        private readonly IUserRepository _userRepository;

        public ServerListService(IServerRepository serverRepository, IUserRepository userRepository)
	    {
		    _serverRepository = serverRepository;
            _userRepository = userRepository;
        }

        public async Task<string> Authenticate(string username, string password)
        {
            var token = await _userRepository.Authenticate(username, password);
            return token;
        }

        public async Task<IEnumerable<Server>> Get()
        {
	        var servers = await _serverRepository.Get();
            return servers;
        }
    }
}
