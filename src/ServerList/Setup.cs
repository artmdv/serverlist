﻿using MvvmCross.Platforms.Wpf.Core;
using MvvmCross.ViewModels;
using ServerList.WPF.Application;

namespace ServerList.WPF
{
    public class Setup: MvxWpfSetup
    {
        protected override IMvxApplication CreateApp()
        {
            return new ServerListApp();
        }
    }
}
