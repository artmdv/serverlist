﻿using System.Windows;
using MvxWpfView = MvvmCross.Platforms.Wpf.Views.MvxWpfView;

namespace ServerList.WPF.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : MvxWpfView
    {
        public LoginView()
        {
            InitializeComponent();
        }

        //This is hack to have placeholder for passwordbox. easiest way to do it, it seems.
        private void PasswordInput_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordPlaceholder.Visibility = Visibility.Visible;
            if (PasswordInput.Password.Length > 0)
            {
                PasswordPlaceholder.Visibility = Visibility.Hidden;
            }
        }
    }
}
