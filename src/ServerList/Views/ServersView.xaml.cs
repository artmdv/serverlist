﻿using MvvmCross.Platforms.Wpf.Views;

namespace ServerList.WPF.Views
{
    /// <summary>
    /// Interaction logic for ServersView.xaml
    /// </summary>
    public partial class ServersView : MvxWpfView
    {
        public ServersView()
        {
            InitializeComponent();
        }
    }
}
