﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using MvvmCross.Navigation;
using NUnit.Framework;
using ServerList.Application.Services;
using ServerList.Domain.Identity;
using ServerList.Domain.Models;
using ServerList.WPF.Application.ViewModels;

namespace SeverList.Tests
{
    [TestFixture]
    public class ViewModelTests
    {
        private Mock<IMvxNavigationService> _navigationMock;
        private Mock<IServerListService> _service;

        [SetUp]
        public void Setup()
        {
            CustomPrincipal customPrincipal = new CustomPrincipal();
            Thread.CurrentPrincipal = customPrincipal;

            _navigationMock = new Mock<IMvxNavigationService>();
            _service = new Mock<IServerListService>();
        }

        [Test]
        public async Task WhenServiceReturnsTokenThenIdentityIsSet()
        {
            _service.Setup(x => x.Authenticate(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync("token");

            var viewModel = new LoginViewModel(_service.Object, _navigationMock.Object);
            await viewModel.LoginCommand.ExecuteAsync();

            var identity = Thread.CurrentPrincipal.Identity as BearerIdentity;

            Assert.IsNotEmpty(identity?.Token);
            Assert.IsNull(viewModel.Error);
        }

        [Test]
        public async Task IfCurrentPrincipalIsNotCustomPrincipalTokenIsNotSet()
        {
            Thread.CurrentPrincipal = null;
            _service.Setup(x => x.Authenticate(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync("token");

            var viewModel = new LoginViewModel(_service.Object, _navigationMock.Object);
            await viewModel.LoginCommand.ExecuteAsync();

            var identity = Thread.CurrentPrincipal?.Identity as BearerIdentity;

            Assert.IsNull(identity?.Token);
            Assert.IsNull(viewModel.Error);
        }

        [Test]
        public async Task IfFailedToAuthenticateErrorIsSet()
        {
            _service.Setup(x => x.Authenticate(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(string.Empty);

            var viewModel = new LoginViewModel(_service.Object, _navigationMock.Object);
            await viewModel.LoginCommand.ExecuteAsync();

            var identity = Thread.CurrentPrincipal.Identity as BearerIdentity;

            Assert.IsNull(identity?.Token);
            Assert.AreEqual("Authentication failed.", viewModel.Error);
        }
        
        [Test]
        public async Task OnSuccessServerGetServersIsPopulated()
        {
            _service.Setup(x => x.Get()).ReturnsAsync(new List<Server>() {new Server("a", 1)});

            var viewModel = new ServersViewModel(_service.Object, _navigationMock.Object);
            await viewModel.Initialize();
            
            Assert.That(viewModel.Servers.Count(), Is.EqualTo(1));
        }

        [Test]
        public async Task WhenLoggedOutIdentityIsSetToAnonymous()
        {
            _service.Setup(x => x.Authenticate(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync("token");

            var viewModel = new LoginViewModel(_service.Object, _navigationMock.Object);
            await viewModel.LoginCommand.ExecuteAsync();

            var loggedInIdentity = Thread.CurrentPrincipal.Identity as BearerIdentity;
            
            var serversViewModel = new ServersViewModel(_service.Object, _navigationMock.Object);
            await serversViewModel.LogoutCommand.ExecuteAsync();

            var loggedOutIdentity = Thread.CurrentPrincipal.Identity as BearerIdentity;

            Assert.IsNotEmpty(loggedInIdentity?.Token);
            Assert.IsNull(viewModel.Error);
            Assert.IsInstanceOf<AnonymousIdentity>(loggedOutIdentity);
        }
    }
}
