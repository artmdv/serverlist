﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using MvvmCross.Navigation;
using NUnit.Framework;
using ServerList.Application.Services;
using ServerList.Domain.Identity;
using ServerList.Domain.Interfaces;
using ServerList.Infrastructure.Repositories;
using ServerList.WPF.Application.ViewModels;

namespace SeverList.Tests
{
    [TestFixture,Explicit]
    public class IntegrationTests
    {
        private Mock<IMvxNavigationService> _navigationMock;
        private IServerRepository _serverRepo;
        private UserRepository _userRepo;
        private ServerListService _service;

        [SetUp]
        public void Setup()
        {
            CustomPrincipal customPrincipal = new CustomPrincipal();
            Thread.CurrentPrincipal = customPrincipal;

            _navigationMock = new Mock<IMvxNavigationService>();

            _serverRepo = new ServerRepository(new HttpClient());

            _userRepo = new UserRepository(new HttpClient());
            
            _service = new ServerListService(_serverRepo, _userRepo);
        }

        [Test]
        public async Task CallingAuthenticateReturnsToken()
        {
            await Login();

            var identity = Thread.CurrentPrincipal.Identity as BearerIdentity;

            Assert.IsNotNull(identity?.Token);
        }

        [Test]
        public async Task CallingAuthenticateWithBadCredentialsSetsAnonymousIdentity()
        {
            var loginViewModel = new LoginViewModel(_service, _navigationMock.Object);
            loginViewModel.Username = "badusername";
            loginViewModel.Password = "bapassword";
            await loginViewModel.LoginCommand.ExecuteAsync();

            Assert.IsInstanceOf<AnonymousIdentity>(Thread.CurrentPrincipal.Identity);
        }

        [Test]
        public async Task GetServersReturnsList()
        {
            await Login();

            var serversViewModel = new ServersViewModel(_service, _navigationMock.Object);

            await serversViewModel.Initialize();

            Assert.IsNotEmpty(serversViewModel.Servers);
        }

        [Test]
        public async Task GetServersWithoutTokenDoesntPopulateServers()
        {
            var serversViewModel = new ServersViewModel(_service, _navigationMock.Object);

            await serversViewModel.Initialize();

            Assert.IsNull(serversViewModel.Servers);
        }

        [Test]
        public async Task WhenLoggedOutIdentityIsSetToAnonymous()
        {
            await Login();
            var serversViewModel = new ServersViewModel(_service, _navigationMock.Object);
            var identity = Thread.CurrentPrincipal.Identity as BearerIdentity;


            Assert.IsNotNull(identity?.Token);
            await serversViewModel.LogoutCommand.ExecuteAsync();

            identity = (BearerIdentity) Thread.CurrentPrincipal.Identity;

            Assert.IsNull(identity.Token);
            Assert.IsInstanceOf<AnonymousIdentity>(identity);
        }

        private async Task Login()
        {
            var loginViewModel = new LoginViewModel(_service, _navigationMock.Object);
            loginViewModel.Username = "tesonet";
            loginViewModel.Password = "partyanimal";
            await loginViewModel.LoginCommand.ExecuteAsync();
        }
    }
}
