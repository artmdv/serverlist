disclaimer: UI is not my strongest suite and I haven't touched WPF for 7 years, that being said- XAML code is far from "production ready".

Everything should be made into style templates, components, etc. Scrollbar copypasta should be cleaned up.

MVVMCross because it looked simpler with better documenation

ServerList.Application is separate from ServerList.WPF.Application to completely separate business layer services from anything UI (including MVVM architecture, specifically ViewModels and MVVMCross framework)

IMHO Binding password directly is not a security risk for demo solution. There are also possibilities to snatch password directly from the input box anyway. I have seen secure string implementations which could be implemented for ideal solution.

this project is just living here
